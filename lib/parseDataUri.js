'use strict';

const crypto = require('crypto');

const DATA_URI_PATTERN = /^data:application\/pkcs8;kid=([\w.\-+/]+);base64,([a-zA-Z0-9+/=]+)$/;

function parsePrivateKey(privateKeyDerBase64) {
    const privateKeyDerBuffer = Buffer.from(privateKeyDerBase64, 'base64');
    const privateKeyObj = crypto.createPrivateKey({
        key: privateKeyDerBuffer,
        type: 'pkcs8',
        format: 'der'
    });
    const privateKeyPemBuffer = privateKeyObj.export({type: 'pkcs8', format: 'pem'});
    return privateKeyPemBuffer.toString('base64');
}

function parseDataUri(dataUri) {
    const match = DATA_URI_PATTERN.exec(decodeURIComponent(dataUri));
    if (!match) {
        throw new Error('Data URI does not match required format!');
    }

    const keyId = match[1];
    const privateKey = parsePrivateKey(match[2]);

    return {
        keyId,
        privateKey
    };
}

module.exports = parseDataUri;
