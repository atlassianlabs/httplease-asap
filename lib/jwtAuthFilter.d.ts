import { JwtConfig } from './types';

// TODO Give this a proper return type once once httplease eventually has better types
declare function createJwtAuthFilter(jwtConfig: JwtConfig): any;

export = createJwtAuthFilter;
