import { JwtConfig, AuthHeaderGenerator } from './types';

declare function createAuthHeaderGenerator(
    jwtConfig: JwtConfig
): AuthHeaderGenerator;

export = createAuthHeaderGenerator;
