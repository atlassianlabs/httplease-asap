'use strict';

const createAuthHeaderGenerator = require('./authHeaderGenerator');

function createJwtAuthFilter(jwtConfig) {

    const getOrGenerateAuthHeader = createAuthHeaderGenerator(jwtConfig);

    return function addJwtAuthHeader(requestConfig, next) {
        requestConfig.httpOptions.headers['authorization'] = getOrGenerateAuthHeader();
        return next(requestConfig);
    };
}

module.exports = createJwtAuthFilter;
