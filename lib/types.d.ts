export interface PrivateKey {
    keyId: string;
    privateKey: string;
}

export interface JwtConfig extends PrivateKey {
    issuer: string;
    audience: string | string[];
    subject?: string;
    tokenExpiryMs?: number;
    tokenMaxAgeMs?: number;
    additionalClaims?: Record<string, unknown>;
}

export type AuthHeaderGenerator = () => string;
