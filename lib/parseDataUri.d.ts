import { PrivateKey } from './types';

declare function parseDataUri(dataUri: string): PrivateKey;

export = parseDataUri;
