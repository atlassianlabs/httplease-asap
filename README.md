# httplease-asap

This is an implementation of [ASAP S2S Authentication](https://s2sauth.bitbucket.io/) as an [httplease](https://bitbucket.org/atlassianlabs/httplease) filter.


# Usage guide

Install the library:

```
npm install --save httplease-asap
```

For more examples have a look at the `test/integration` directory.


## Simple request

```
const httplease = require('httplease');
const httpleaseAsap = require('httplease-asap');


const jwtConfig = {
    privateKey: 'private key in pem format',
    issuer: 'an-issuer',
    keyId: 'the-keyid',
    audience: 'an-audience'
};

// this can be saved and reused as many times as you want
const httpClient = httplease.builder()
    .withBaseUrl('http://example.com/basePath')
    .withFilter(httpleaseAsap.createJwtAuthFilter(jwtConfig));


// make a request
httpClient
    .withPath('/post')
    .withMethodGet()
    .send();
```


## jwtConfig from data URI

```
const httpleaseAsap = require('httplease-asap');

const dataUri = 'data:application/pkcs8;kid=an-issuer%2Fthe-keyid;base64,private key in base64 DER format';

const jwtConfig = Object.assign({
    issuer: 'an-issuer',
    audience: 'an-audience'
}, httpleaseAsap.parseDataUri(dataUri));
```


## Just generate an auth header

The same `jwtConfig` format can be used to create a httplease filter or just a function which when called returns an authorization header.
```
const httpleaseAsap = require('httplease-asap');

const jwtConfig = {
    privateKey: 'private key in pem format',
    issuer: 'an-issuer',
    keyId: 'the-keyid',
    audience: 'an-audience',
    tokenExpiryMs: 10 * 60 * 1000, // Optional, max is 1 hour. This is how long the generated token stays valid.
    tokenMaxAgeMs: 9 * 60 * 1000 // Optional, must be less than tokenExpiryMs. How long to cache the token.
};

const authHeaderGenerator = httpleaseAsap.createAuthHeaderGenerator(jwtConfig);

const authHeader = authHeaderGenerator();

console.log(authHeader); // Bearer abcd1234
```

## Additional claims

Just add them to your `jwtConfig` and use them with either `createJwtAuthFilter` or `createAuthHeaderGenerator`.

```
const jwtConfig = {
    // the usual stuff
    additionalClaims: {
        myCustomClaim: 'hello world'
    }
}
```


# Development guide

## Install dependencies

```
npm install
```


## Useful commands

```
# Run all checks
npm test

# Run just the jasmine tests
npm run test:jasmine

# Run just the linter
npm run test:lint
```


## Perform a release

```
npm version 99.98.97
npm publish
git push
git push --tags
```

## Contributors

Pull requests, issues and comments welcome. For pull requests:

* Add tests for new features and bug fixes
* Follow the existing style
* Separate unrelated changes into multiple pull requests

See the existing issues for things to start contributing.

For bigger changes, make sure you start a discussion first by creating an issue and explaining the intended change.

Atlassian requires contributors to sign a Contributor License Agreement, known as a CLA. This serves as a record stating that the contributor is entitled to contribute the code/documentation/translation to the project and is willing to have it used in distributions and derivative works (or is willing to transfer ownership).

* [CLA for corporate contributors](https://na2.docusign.net/Member/PowerFormSigning.aspx?PowerFormId=e1c17c66-ca4d-4aab-a953-2c231af4a20b)
* [CLA for individuals](https://na2.docusign.net/Member/PowerFormSigning.aspx?PowerFormId=3f94fbdc-2fbe-46ac-b14c-5d152700ae5d)


# License

Copyright (c) 2016 Atlassian and others.
Apache 2.0 licensed, see [LICENSE](LICENSE) file.
