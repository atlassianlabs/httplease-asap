'use strict';

const jsonWebToken = require('jsonwebtoken');

const jwtKeySupport = require('../support/jwtKeySupport');

function parseAuthHeader(authHeader) {
    const parts = authHeader.split(' ');
    const scheme = parts[0];
    const token = parts[1];
    expect(scheme).toBe('Bearer');

    return jsonWebToken.verify(token, jwtKeySupport.publicKey);
}

module.exports = parseAuthHeader;
