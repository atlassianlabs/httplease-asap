'use strict';

const privateKeyPem = `
-----BEGIN PRIVATE KEY-----
MIIEvgIBADANBgkqhkiG9w0BAQEFAASCBKgwggSkAgEAAoIBAQCgoPbEeg5zhTJA
kuL800G4bFyOEpHLNAQ5+9wXRkvk68PD/F9twGFzluWlKznKXmvpZ9z8Ky6Yge6S
tw1pNxratM6fWPvflzRTpa2IcCSYfNPG7+VO1AwW7Us+5QXDa2THZ5DTeEo77qfF
naZfRBxDQSD6lPg7YE7AvE0Jab5zYlG0xpPZ2DZpWX11cvEwstuIDhEVkfTUuwIL
/88D+uyoj6izwK/cWy93G6ciitEQryDE/nqduuFvpFE/gNXU4xPJSnUHN5DNHnxf
tDxkznRviQs3G3QV8FoxbbF8TxCRQgNHG5ZQX2AS7Hzmyi10aobbqKsuHjjVfeV9
ldAEX7I1AgMBAAECggEADxvk6klC17nlJq7KXBaw/Vxv/ksf3JnJdfzLUIMju2Yz
1VkFTukEIZ/FfRpKRs9H3j4PSsbzDJRYMQO35rgwtfbA4rs3geoSmXWhk45c2t0O
/IzvKFGIy5VJeSKIvr94zMrglAuiC+4gaNWx4w8gi3DyXa+qg8dE0RtFMcf5CphE
QsVP8rc5nPgT8RifujTDIusN4zK8GIAxsMf8j5KRbg2LpqfMtM0tNEGTSKjZMMe6
kGfTh+uflCcMpx96XGX/n04Bp8uQa/uEWD8kHdi9o3cJm+vvixou6I3wdPCD0rFu
Tlhk/YnWl5Me8rscslzLmvJ+c9mb8J5UENu6/DtcIQKBgQDS06XzEoKcKH21XTO6
Gk5O8Zb20vu27qCo5syIvIH/1oPhnzI3+yx42vSwHM0iOAAaPAeaKUqlllE+vCn1
8E2WI8XQYmWZMEAXSMJWeqKpmhvjYVNDJljAV6MXDBghNyn29q8qC2AaTGaT1qb8
faTLzn8hn9CR3uDr8aV6/8awEQKBgQDDC9YI036zo8VBnpZueOXafKsalPPqv/KD
y7MquQvfRv17yBBVGHtlDTNr9LAr/GwGwgH8+XJPav/w7i+MUp5V3uzmrodVDI+k
3ahQ5EJdC6zwkBiR/EpWZjRKtZv2QfnwtQ791Aw+ouIUwbra20KmBzLn9KrkOoVx
A+a0PlQD5QKBgG52WoLrdeegB42ZUdr2oYUXbAPTM/P7P85/bzdNnceQe/UhVkyi
nACOqJoF3S1KrjMQtOL/M49jHRqDuHCWAfDiuWifxZ/n+gy5IKnzA3tdtbMDcX7D
fsj4ogkXWyU8JA6z3b9nymi9Exnu4/tzUX8/qLjTZZKodqzpJc02asIhAoGBAII5
690WwNjWNsT6SrxV0joP1Po5i6YoJJbksaqLxKZ+Mo6r5GO8pd6xcqzYwauMrN/S
NxudvTnTWjGWBn8tRnRVFF1tGASsD1hnepMP5FNVkVFJFG6cQVHwiWd8XDaOlH+n
k+vb4ZUMkeCD/OtVS+moks+WZ2dcv23Wyfl8SAIVAoGBAMlqEtTLE76Gh+/hxSND
6LsCug8L8Q//ZLIxuhVOH4QC2kBV9j4NXulEYnfieclNcu8BXxa3e37so8IKq3qg
mFUxbIu7kSEVMYmvb9vgTSVSDD2E+4jNBeMzasV2LxF/yXY8Ex1WrYm+UIleFJr1
iWhh4PDFtbCKZvGFYrLpKZmJ
-----END PRIVATE KEY-----
`.trim();

const privateKeyDataUri = `data:application/pkcs8;kid=an-issuer%2Fthe-keyid;base64,${
    privateKeyPem.split('\n').slice(1, -1).join('')
}`;

const publicKey = `
-----BEGIN PUBLIC KEY-----
MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAoKD2xHoOc4UyQJLi/NNB
uGxcjhKRyzQEOfvcF0ZL5OvDw/xfbcBhc5blpSs5yl5r6Wfc/CsumIHukrcNaTca
2rTOn1j735c0U6WtiHAkmHzTxu/lTtQMFu1LPuUFw2tkx2eQ03hKO+6nxZ2mX0Qc
Q0Eg+pT4O2BOwLxNCWm+c2JRtMaT2dg2aVl9dXLxMLLbiA4RFZH01LsCC//PA/rs
qI+os8Cv3FsvdxunIorREK8gxP56nbrhb6RRP4DV1OMTyUp1BzeQzR58X7Q8ZM50
b4kLNxt0FfBaMW2xfE8QkUIDRxuWUF9gEux85sotdGqG26irLh441X3lfZXQBF+y
NQIDAQAB
-----END PUBLIC KEY-----
`.trim();

module.exports = {
    privateKeyPem,
    privateKeyDataUri,
    publicKey
};
