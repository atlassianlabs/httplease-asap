'use strict';

const createAuthHeaderGenerator = require('../../lib/authHeaderGenerator');
const jwtKeySupport = require('../support/jwtKeySupport');
const parseAuthHeader = require('../support/parseAuthHeader');

describe('authHeaderGenerator', () => {

    const GlobalDate = global.Date;

    let jwtConfig;
    let MockDate;
    let currentTime;

    beforeEach(() => {
        jwtConfig = {
            privateKey: jwtKeySupport.privateKeyPem,
            issuer: 'an-issuer',
            keyId: 'the-keyid',
            audience: 'an-audience'
        };

        currentTime = 123456789 * 1000;
        MockDate = function MockDate() {
            return new GlobalDate(currentTime);
        };
        MockDate.now = jasmine.createSpy('MockDateNow');
        MockDate.now.and.callFake(() => currentTime);
        global.Date = MockDate;
    });

    afterEach(() => {
        global.Date = GlobalDate;
    });

    function generateToken(generator=createAuthHeaderGenerator(jwtConfig)) {
        const authHeader = generator();
        const token = parseAuthHeader(authHeader);
        return token;
    }

    it('has iss claim', () => {
        const token = generateToken();

        expect(token.iss).toBe('an-issuer');
    });

    it('has sub claim set to issuer if not provided', () => {
        const token = generateToken();

        expect(token.sub).toBe('an-issuer');
    });

    it('has sub claim set to provided value', () => {
        jwtConfig.subject = 'a-subject';

        const token = generateToken();

        expect(token.sub).toBe('a-subject');
    });

    it('has aud claim', () => {
        const token = generateToken();

        expect(token.aud).toBe('an-audience');
    });

    it('has jti claim', () => {
        const token = generateToken();

        expect(token.jti).toBeDefined();
    });

    it('has iat claim set to the current time', () => {
        const token = generateToken();

        expect(token.iat).toBe(123456789);
    });

    it('has nbf claim set to the current time', () => {
        const token = generateToken();

        expect(token.nbf).toBe(123456789);
    });

    it('has exp claim set to 10 minutes after the current time', () => {
        const token = generateToken();

        expect(token.exp).toBe(123456789 + 600);
    });

    it('has exp claim set to custom expiry time', () => {
        jwtConfig.tokenExpiryMs = 42000;

        const token = generateToken();

        expect(token.exp).toBe(123456789 + 42);
    });

    it('uses the same Date.now() for all calculations to ensure sanity', () => {
        // in particular we always want: iat <= nbf
        createAuthHeaderGenerator(jwtConfig);

        expect(MockDate.now.calls.count()).toBe(1);
    });

    it('removes newlines and quotes from the private key', () => {
        jwtConfig.privateKey = `"\\n${jwtConfig.privateKey}\\n"`;

        const token = generateToken();

        expect(token).toBeDefined();
    });

    it('generates the same token if called twice within 9 minutes', () => {
        const generator = createAuthHeaderGenerator(jwtConfig);

        const firstToken = generateToken(generator);
        currentTime += 9 * 60 * 1000;
        const secondToken = generateToken(generator);

        expect(firstToken).toEqual(secondToken);
    });

    it('generates unique tokens if called after 9 minutes', () => {
        const generator = createAuthHeaderGenerator(jwtConfig);

        const firstToken = generateToken(generator);
        currentTime += 9 * 60 * 1000 + 1;
        const secondToken = generateToken(generator);

        expect(firstToken).not.toEqual(secondToken);
    });

    it('generates the same token if called twice within custom age', () => {
        jwtConfig.tokenMaxAgeMs = 10 * 60 * 1000 - 1;
        const generator = createAuthHeaderGenerator(jwtConfig);

        const firstToken = generateToken(generator);
        currentTime += 10 * 60 * 1000 - 1;
        const secondToken = generateToken(generator);

        expect(firstToken).toEqual(secondToken);
    });

    it('fails if missing keyId', () => {
        delete jwtConfig.keyId;

        expect(() => createAuthHeaderGenerator(jwtConfig))
            .toThrowError(/jwtConfig.keyId must be set/);
    });

    it('fails if missing issuer', () => {
        delete jwtConfig.issuer;

        expect(() => createAuthHeaderGenerator(jwtConfig))
            .toThrowError(/jwtConfig.issuer must be set/);
    });

    it('fails if missing privateKey', () => {
        delete jwtConfig.privateKey;

        expect(() => createAuthHeaderGenerator(jwtConfig))
            .toThrowError(/jwtConfig.privateKey must be set/);
    });

    it('fails if invalid privateKey', () => {
        jwtConfig.privateKey = 'this is not a valid private key';

        expect(() => createAuthHeaderGenerator(jwtConfig))
            .toThrowError('secretOrPrivateKey must be an asymmetric key when using RS256');
    });

    it('fails if missing audience', () => {
        delete jwtConfig.audience;

        expect(() => createAuthHeaderGenerator(jwtConfig))
            .toThrowError(/audience must be set/);
    });

    it('supports additional claims', () => {
        jwtConfig.additionalClaims = {
            myCustomClaim: 'foo bar'
        };

        const token = generateToken();

        expect(token.myCustomClaim).toBe('foo bar');
    });

});
