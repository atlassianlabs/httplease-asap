'use strict';

const createAuthHeaderGenerator = require('../../lib/authHeaderGenerator');
const jwtKeySupport = require('../support/jwtKeySupport');
const parseAuthHeader = require('../support/parseAuthHeader');
const parseDataUri = require('../../lib/parseDataUri');

describe('parseDataUri', () => {

    function generateToken(dataUri) {
        const jwtConfig = Object.assign({
            issuer: 'an-issuer',
            audience: 'an-audience'
        }, parseDataUri(dataUri));

        const generator = createAuthHeaderGenerator(jwtConfig);
        const authHeader = generator();
        const token = parseAuthHeader(authHeader);
        return token;
    }

    it('generates verifiable token', () => {
        const token = generateToken(jwtKeySupport.privateKeyDataUri);

        expect(token.iss).toBe('an-issuer');
    });

    it('parses the key ID', () => {
        const keyId = parseDataUri(jwtKeySupport.privateKeyDataUri).keyId;

        expect(keyId).toBe('an-issuer/the-keyid');
    });

    it('rejects with invalid prefix', () => {
        const fn = () => generateToken('not a data uri');

        expect(fn).toThrowError(/does not match required format/);
    });

    it('rejects with invalid kid= attribute', () => {
        const fn = () => generateToken('data:application/pkcs8;bad=something');

        expect(fn).toThrowError(/does not match required format/);
    });

    it('rejects with non base64 encoding', () => {
        const fn = () => generateToken('data:application/pkcs8;kid=something;notbase64');

        expect(fn).toThrowError(/does not match required format/);
    });

    it('rejects with invalid key base64 data', () => {
        const fn = () => generateToken('data:application/pkcs8;kid=something;base64,notbase64!');

        expect(fn).toThrowError(/does not match required format/);
    });

    it('rejects with invalid key data', () => {
        const fn = () => generateToken('data:application/pkcs8;kid=something;base64,YWJjZA==');

        expect(fn).toThrowError(/Too few bytes to read ASN\.1 value|not enough data/);
    });

});
