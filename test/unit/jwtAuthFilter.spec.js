'use strict';

const createJwtAuthFilter = require('../../lib/jwtAuthFilter');
const jwtKeySupport = require('../support/jwtKeySupport');

describe('jwtAuthFilter', () => {
    let fakeResponse;
    let fakeNext;
    let requestConfig;
    let jwtConfig;

    beforeEach(() => {
        fakeResponse = {
            body: 'body',
            headers: {},
            status: 200
        };

        requestConfig = {
            httpOptions: {
                headers: {}
            }
        };

        jwtConfig = {
            privateKey: jwtKeySupport.privateKeyPem,
            issuer: 'an-issuer',
            keyId: 'the-keyid',
            audience: 'an-audience'
        };

        fakeNext = jasmine.createSpy('next');
        fakeNext.and.returnValue(Promise.resolve(fakeResponse));
    });

    it('returns result of calling next()', () => {
        const filter = createJwtAuthFilter(jwtConfig);

        return filter(requestConfig, fakeNext)
            .then((response) => {
                expect(fakeNext).toHaveBeenCalledWith(requestConfig);
                expect(response).toBe(fakeResponse);
            });
    });

    it('injects an Authorization header', () => {
        const filter = createJwtAuthFilter(jwtConfig);

        return filter(requestConfig, fakeNext)
            .then(() => {
                const authHeader = requestConfig.httpOptions.headers['authorization'];
                expect(authHeader).toMatch(/^Bearer /);
            });
    });

});
