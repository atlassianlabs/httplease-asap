'use strict';

const {promisify} = require('util');
const express = require('express');
const http = require('http');
const httplease = require('httplease');
const {
    createAsapAuthenticationMiddleware,
    createAsapIssuerWhitelistMiddleware
} = require('express-asap');

const createJwtAuthFilter = require('../../index').createJwtAuthFilter;
const parseDataUri = require('../../index').parseDataUri;
const jwtKeySupport = require('../support/jwtKeySupport');

const PORT = 8082;
const AUDIENCE = 'an-audience';
const ISSUER = 'an-issuer';
const KEY_ID = 'an-issuer/the-keyid';

function configureExpressApp() {
    const app = express();

    const authorizedIssuers = [ISSUER];

    app.get(
        '^/resource',
        createAsapAuthenticationMiddleware({
            publicKeyBaseUrls: [`http://localhost:${PORT}/`],
            resourceServerAudience: AUDIENCE
        }),
        createAsapIssuerWhitelistMiddleware(authorizedIssuers),
        (req, res) => res.status(200).send('Auth ok!')
    );

    app.get(`^/${KEY_ID}`, (req, res) => res.status(200).send(jwtKeySupport.publicKey));

    // eslint-disable-next-line max-params,no-unused-vars
    app.use((err, req, res, next) => {
        res.status(err.statusCode).json(err);
    });

    return app;
}


describe('integration/jwtAuthFilter', () => {

    let server;
    let jwtConfig;
    let httpClient;

    beforeAll(async () => {
        const app = configureExpressApp();
        server = http.createServer(app);
        await promisify(server.listen.bind(server))(PORT);
    });

    afterAll(async () => {
        await promisify(server.close.bind(server))();
    });

    beforeEach(() => {
        jwtConfig = {
            privateKey: jwtKeySupport.privateKeyPem,
            issuer: ISSUER,
            keyId: KEY_ID,
            audience: AUDIENCE
        };

        httpClient = httplease.builder()
            .withMethodGet()
            .withExpectStatus([200])
            .withBufferBodyResponseHandler()
            .withTimeout(5000)
            .withBaseUrl(`http://localhost:${PORT}`)
            .withPath('/resource');
    });

    it('pem private key should get an OK response from the server', () => {
        return httpClient
            .withFilter(createJwtAuthFilter(jwtConfig))
            .send()
            .then((response) => {
                expect(response.body.toString()).toBe('Auth ok!');
            });
    });

    it('should get a 401 from the server if we send the wrong audience', () => {
        jwtConfig.audience = 'some-wrong-audience';

        const promise = httpClient
            .withFilter(createJwtAuthFilter(jwtConfig))
            .send();

        return expectToReject(promise)
            .then((err) => {
                expect(err.response).toBeDefined();
                expect(err.response.statusCode).toBe(401);
            });
    });

    it('data URI should get an OK response from server', () => {
        jwtConfig = Object.assign({
            issuer: ISSUER,
            audience: AUDIENCE
        }, parseDataUri(jwtKeySupport.privateKeyDataUri));

        return httpClient
            .withFilter(createJwtAuthFilter(jwtConfig))
            .send()
            .then((response) => {
                expect(response.body.toString()).toBe('Auth ok!');
            });
    });

});
